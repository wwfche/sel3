from selenium import webdriver
#from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait

def test_yandex_search():
    driver = webdriver.Chrome()
    driver.get("https://ya.ru/")
    search_input = driver.find_element_by_id("text").send_keys("yandex.market.ru")
    search_button = driver.find_element_by_class_name("search2__button").click()

    def chek_result_count(driver):
        search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
        return len(search_results) == 10

    WebDriverWait(driver, 5, 0.5).until(chek_result_count)


