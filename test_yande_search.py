from selenium import webdriver
#from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait

def test_yandex_search():
    driver = webdriver.Chrome()
    driver.get("https://ya.ru/")
    search_input = driver.find_element_by_id("text").send_keys("yandex.market.ru")
    search_button = driver.find_element_by_class_name("search2__button").click()


    search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')


    assert len(search_results) == 10


